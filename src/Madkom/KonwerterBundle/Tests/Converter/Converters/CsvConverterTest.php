<?php

namespace Madkom\KonwerterBundle\Tests\Converters;

use Madkom\KonwerterBundle\Converter\Converters\CsvConverter;

class CsvConverterTest extends AbstractConverterTest
{
    /** @var CsvConverter */
    private $csvConverter;

    protected function setUp()
    {
        parent::setUp();

        $this->csvConverter = $this->container->get('madkom_konwerter.csv');
    }

    public function testGetName()
    {
        $this->assertEquals('CSV', $this->csvConverter->getName());
    }

    public function testDoesInputMatchForType()
    {
        foreach ($this->inputTypes['csv'] as $inputType) {
            $this->assertTrue($this->csvConverter->doesInputMatchForType($inputType));
        }
    }

    public function testConvertToCommonType()
    {
        foreach ($this->inputTypes['csv'] as $delimiterName => $inputType) {
            $commonType = $this->csvConverter->convertToCommonType($inputType);
            $this->assertInternalType('array', $commonType);
            $this->assertEquals(
                $this->inputTypes['common'],
                $commonType,
                "Invalid common type for {$delimiterName} delimiter"
            );
        }
    }

    public function testConvertToConverterType()
    {
        $possibleDelimiters = $this->csvConverter->getPossibleDelimiters();
        foreach ($this->inputTypes['csv'] as $delimiterName => $inputType) {
            $this->csvConverter->setDelimiter($possibleDelimiters[$delimiterName]);
            $targetType = $this->csvConverter->convertToConverterType($this->inputTypes['common']);
            $this->assertEquals(
                $targetType,
                $this->inputTypes['csv'][$delimiterName],
                "Invalid csv type for {$delimiterName} delimiter"
            );
        }
    }
}
 