<?php

namespace Madkom\KonwerterBundle\Converter\Converters;

use Madkom\KonwerterBundle\Converter\ConverterInterface;

abstract class AbstractConverter implements ConverterInterface
{
    /**
     * Nazwa konwertera
     *
     * @var string
     */
    protected $name;

    /**
     * Pobieranie nazwy konwertera.
     *
     * @return mixed
     * @throws \Exception gdy puste name
     */
    final public function getName()
    {
        if (empty($this->name)) {
            throw new \Exception('Konwerter musi miec nazwe - uzyj `setName($name)`');
        }

        return $this->name;
    }

    /**
     * Ustawienie nazwy konwertera.
     *
     * @param $name
     */
    final public function setName($name)
    {
        $this->name = $name;
    }
}