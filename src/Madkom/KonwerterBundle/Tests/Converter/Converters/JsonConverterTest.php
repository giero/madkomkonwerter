<?php

namespace Madkom\KonwerterBundle\Tests\Converters;

use Madkom\KonwerterBundle\Converter\Converters\JsonConverter;

class JsonConverterTest extends AbstractConverterTest
{
    /** @var JsonConverter */
    private $jsonConverter;

    protected function setUp()
    {
        parent::setUp();

        $this->jsonConverter = $this->container->get('madkom_konwerter.json');
    }

    public function testGetName()
    {
        $this->assertEquals('Json', $this->jsonConverter->getName());
    }

    public function testDoesInputMatchForType()
    {
        $this->assertTrue($this->jsonConverter->doesInputMatchForType($this->inputTypes['json']));
    }

    public function testConvertToCommonType()
    {
        $commonType = $this->jsonConverter->convertToCommonType($this->inputTypes['json']);
        $this->assertInternalType('array', $commonType);
        $this->assertEquals($this->inputTypes['common'], $commonType);
    }

    public function testConvertToConverterType()
    {
        $targetType = $this->jsonConverter->convertToConverterType($this->inputTypes['common']);
        $this->assertJsonStringEqualsJsonString($this->inputTypes['json'], $targetType);
    }
}
 