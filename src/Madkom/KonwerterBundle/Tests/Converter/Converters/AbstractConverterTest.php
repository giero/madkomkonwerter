<?php

namespace Madkom\KonwerterBundle\Tests\Converters;

use Madkom\KonwerterBundle\Tests\KernelAwareTest;

abstract class AbstractConverterTest extends KernelAwareTest
{
    protected $inputTypes = array();

    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        parent::setUp();

        $this->inputTypes['common'] = unserialize(
            $this->getFixtureContent('serializedCommonType')
        );
        $this->inputTypes['xml'] = $this->getFixtureContent('xmlInput.xml');
        $this->inputTypes['json'] = $this->getFixtureContent('jsonType.json');
        $this->inputTypes['html'] = $this->getFixtureContent('tableHtmlInput.html');
        $this->inputTypes['ascii'] = $this->getFixtureContent('tableASCIIType');
        $this->inputTypes['ul.li'] = $this->getFixtureContent('ulLiHtmlInput.html');
        $this->inputTypes['csv'] = array(
            'carete' => $this->getFixtureContent('csvCareteType.csv'),
            'pipe' => $this->getFixtureContent('csvPipeType.csv'),
            'semicolon' => $this->getFixtureContent('csvSemiColonType.csv'),
            'comma' => $this->getFixtureContent('csvCommaType.csv'),
            'tab' => $this->getFixtureContent('csvTabType.csv'),
        );
    }

    /**
     * Pobiera zawartosc pliku z danymi wejsciowymi/wyjsciowymi dla konwertera
     *
     * @param string $fileName
     * @return string
     */
    private function getFixtureContent($fileName)
    {
        $fixturesPath = __DIR__ . '/../../../Resources/tests/fixtures/';

        return file_get_contents($fixturesPath . $fileName);
    }
}