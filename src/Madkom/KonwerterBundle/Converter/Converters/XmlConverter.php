<?php

namespace Madkom\KonwerterBundle\Converter\Converters;

use Madkom\KonwerterBundle\Lib\Array2XML;
use Madkom\KonwerterBundle\Lib\XML2Array;

class XmlConverter extends AbstractConverter
{
    public function doesInputMatchForType($inputData)
    {
        libxml_use_internal_errors(true);

        $patternTimesMatches = preg_match('@^\<\?xml.+\?\>@', $inputData);

        return false !== $patternTimesMatches &&
        $patternTimesMatches > 0 &&
        false !== simplexml_load_string($inputData);
    }

    public function convertToCommonType($inputData)
    {
        return XML2Array::createArray($inputData);
    }

    public function convertToConverterType($commonType)
    {
        $xml = Array2XML::createXML(key($commonType), reset($commonType));

        return $xml->saveXML();
    }
}