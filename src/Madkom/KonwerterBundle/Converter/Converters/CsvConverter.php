<?php

namespace Madkom\KonwerterBundle\Converter\Converters;

class CsvConverter extends AbstractConverter
{
    private $delimiter = ';';
    /**
     * Delimitery na podstawie http://data-gov.tw.rpi.edu/wiki/CSV_files_use_delimiters_other_than_commas
     * @var array
     */
    private $possibleDelimiters = array(
        'comma' => ',',
        'semicolon' => ';',
        'pipe' => '|',
        'tab' => "\t",
        'carete' => '^'
    );

    public function getPossibleDelimiters()
    {
        return $this->possibleDelimiters;
    }

    public function setDelimiter($delimiter)
    {
        if (in_array($delimiter, $this->possibleDelimiters)) {
            $this->delimiter = $delimiter;
        } else {
            throw new \InvalidArgumentException("Nieprawidłowy delimiter: {$delimiter} - akceptowalne: " . implode(
                ', ',
                $this->possibleDelimiters
            ));
        }
    }

    public function doesInputMatchForType($inputData)
    {
        $delimiter = $this->getCsvDelimiter($inputData);

        return !empty($delimiter);
    }

    public function convertToCommonType($inputData)
    {
        $this->delimiter = '^';
        $csvDelimiter = $this->getCsvDelimiter($inputData);

        $data = preg_split("/((\r?\n)|(\r\n?))/", $inputData); // parsujemy po każdym wierszu
        foreach ($data as $i => &$row) {
            if (empty($row)) {
                unset($data[$i]);
            } else {
                $row = explode($csvDelimiter, trim($row, '"')); // parsujemy po elementach w wierszu

            }
        }
        unset($row);

        $commonType = array('table' => array('row' => array()));
        foreach ($data as &$row) {
            $commonType['table']['row'][] = array('column' => $row);
        }
        unset($row);

        return $commonType;
    }

    public function convertToConverterType($commonType)
    {
        $csvType = '';
        foreach ($commonType['table']['row'] as $row) {
            $csvType .= implode($this->delimiter, $row['column']) . PHP_EOL;
        }

        return $csvType;
    }

    private function getCsvDelimiter($inputData)
    {
        $enclosure = false !== strpos($inputData, '"') ? '\"' : '';
        $inputDataDelimiter = $this->findDelimiterByRegex($inputData, $enclosure);

        return $inputDataDelimiter;
    }

    private function findDelimiterByRegex($inputData, $enclosure = '')
    {
        $matchingDelimiter = null;
        $inputDataFirstRow = strstr($inputData, "\n", true);
        foreach ($this->getPossibleDelimiters() as $delimiter) {
            $delimiter = str_replace(array('^', '|'), array('\^', '\|'), $delimiter);
            $patternTimesMatches = preg_match(
                "/([^{$enclosure}{$delimiter}{$enclosure}]+{$enclosure}{$delimiter}{$enclosure})+[^{$enclosure}{$delimiter}{$enclosure}]+/",
                $inputDataFirstRow
            );
            if (false !== $patternTimesMatches && $patternTimesMatches > 0) {
                if (null !== $matchingDelimiter) {
                    throw new \InvalidArgumentException('Nieprawidłowy format CSV - niejednoznaczny separator');
                }
                $matchingDelimiter = str_replace(array('\^', '\|'), array('^', '|'), $delimiter);
            }
        }

        $enclosure = str_replace('\\', '', $enclosure);

        return $enclosure . $matchingDelimiter . $enclosure;
    }
}