<?php

namespace Madkom\KonwerterBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ConversionType extends AbstractType
{
    private $typeOptions = array();

    public function setTypeOptions($typeOptions)
    {
        $this->typeOptions = $typeOptions;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('data', 'textarea', array())
            ->add('type', 'choice', array(
                'choices' => $this->typeOptions,
                'empty_value' => 'Wybierz typ docelowy',
                'empty_data'  => null

            ))
            ->add('save', 'submit');
    }

    public function getName()
    {
        return 'conversion';
    }
} 