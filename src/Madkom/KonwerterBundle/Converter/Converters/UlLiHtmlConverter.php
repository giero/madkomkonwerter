<?php

namespace Madkom\KonwerterBundle\Converter\Converters;

use Madkom\KonwerterBundle\Lib\Array2XML;
use Madkom\KonwerterBundle\Lib\XML2Array;

class UlLiHtmlConverter extends AbstractConverter
{
    public function doesInputMatchForType($inputData)
    {
        $patternTimesMatches = preg_match(
            '@^<ul>.*<li>.*<ul>.*<li>.+</li>.*</ul>.*</li>.*</ul>$@',
            trim(preg_replace('/\s+/', ' ', $inputData))
        );

        return false !== $patternTimesMatches && $patternTimesMatches > 0;
    }

    public function convertToCommonType($inputData)
    {
        $inputData = preg_replace('@\<li\>\s*\<ul\>@', '<tr>', $inputData);
        $inputData = preg_replace('@\</ul\>\s*\</li\>@', '</tr>', $inputData);
        $inputData = str_replace('ul>', 'table>', $inputData);
        $inputData = str_replace('li>', 'td>', $inputData);
        $commonHtmlType = XML2Array::createArray($inputData);
        $serializedCommonType = str_replace(
            array('tr', 'td'),
            array('row', 'column'),
            json_encode($commonHtmlType)
        );

        return json_decode($serializedCommonType, true);
    }

    public function convertToConverterType($commonType)
    {
        $xml = Array2XML::createXML(key($commonType), reset($commonType));
        $tableHtmlAsXml = preg_replace(
            array('@\<\?.+\?\>@', '@(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+@'), // usuwamy znaczniki xml i puste linie
            array('', ''),
            $xml->saveXML()
        );

        $ulLiHtmlAsXml = str_replace('column>', 'li>', $tableHtmlAsXml);
        $ulLiHtmlAsXml = str_replace('table>', 'ul>', $ulLiHtmlAsXml);
        $ulLiHtmlAsXml = str_replace('<row>', '<li><ul>', $ulLiHtmlAsXml);
        $ulLiHtmlAsXml = str_replace('</row>', '</ul></li>', $ulLiHtmlAsXml);

        $commonHtmlType = XML2Array::createArray($ulLiHtmlAsXml);
        $xml = Array2XML::createXML(key($commonHtmlType), reset($commonHtmlType));

        return preg_replace(
            array('@\<\?.+\?\>@', '@(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+@'), // usuwamy znaczniki xml i puste linie
            array('', ''),
            $xml->saveXML()
        );
    }
}