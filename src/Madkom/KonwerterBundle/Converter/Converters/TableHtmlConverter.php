<?php

namespace Madkom\KonwerterBundle\Converter\Converters;

use Madkom\KonwerterBundle\Lib\Array2XML;
use Madkom\KonwerterBundle\Lib\XML2Array;

class TableHtmlConverter extends AbstractConverter
{
    public function doesInputMatchForType($inputData)
    {
        $patternTimesMatches = preg_match(
            '@^<table>.*<tr>.*<td>.*</td>.*</tr>.*</table>$@',
            trim(preg_replace('/\s+/', ' ', $inputData))
        );

        return false !== $patternTimesMatches && $patternTimesMatches > 0;
    }

    public function convertToCommonType($inputData)
    {
        $commonHtmlType = XML2Array::createArray($inputData);
        $serializedCommonType = str_replace(
            array('tr', 'td'),
            array('row', 'column'),
            json_encode($commonHtmlType)
        );

        return json_decode($serializedCommonType, true);
    }

    public function convertToConverterType($commonType)
    {
        $xml = Array2XML::createXML(key($commonType), reset($commonType));
        $tableHtmlAsXml = preg_replace(
            array('@\<\?.+\?\>@', '@(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+@'), // usuwamy znaczniki xml i puste linie
            array('', ''),
            $xml->saveXML()
        );

        return str_replace(
            array('row', 'column'),
            array('tr', 'td'),
            $tableHtmlAsXml
        );
    }
}