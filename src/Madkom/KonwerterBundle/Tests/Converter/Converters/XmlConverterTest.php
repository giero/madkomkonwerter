<?php

namespace Madkom\KonwerterBundle\Tests\Converters;

use Madkom\KonwerterBundle\Converter\Converters\XmlConverter;

class XmlConverterTest extends AbstractConverterTest
{
    /** @var XmlConverter */
    private $xmlConverter;

    protected function setUp()
    {
        parent::setUp();

        $this->xmlConverter = $this->container->get('madkom_konwerter.xml');
    }

    public function testGetName()
    {
        $this->assertEquals('XML', $this->xmlConverter->getName());
    }

    public function testDoesInputMatchForType()
    {
        $this->assertTrue($this->xmlConverter->doesInputMatchForType($this->inputTypes['xml']));
    }

    public function testConvertToCommonType()
    {
        $commonType = $this->xmlConverter->convertToCommonType($this->inputTypes['xml']);
        $this->assertInternalType('array', $commonType);
        $this->assertEquals($this->inputTypes['common'], $commonType);
    }

    public function testConvertToConverterType()
    {
        $targetType = $this->xmlConverter->convertToConverterType($this->inputTypes['common']);
        $this->assertXmlStringEqualsXmlString($targetType, $this->inputTypes['xml']);
    }
}
 