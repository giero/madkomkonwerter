<?php

namespace Madkom\KonwerterBundle\Converter\Converters;

class TableAsciiConverter extends AbstractConverter
{
    public function doesInputMatchForType($inputData)
    {
        $patternTimesMatches = preg_match(
            '@^\+(-+\+)+@',
            strstr($inputData, "\n", true) // pierwszy wiersz
        );

        return false !== $patternTimesMatches && $patternTimesMatches > 0;
    }

    public function convertToCommonType($inputData)
    {
        $data = preg_split("/((\r?\n)|(\r\n?))/", $inputData); // parsujemy po każdym wierszu
        $columns = array();
        foreach ($data as $i => &$row) {
            if (empty($row) || preg_match('@^\+(-+\+)+@', $row)) {
                unset($data[$i]);
            } else {
                $columns[$i] = array();
                foreach (explode(' | ', trim($row, '| ')) as $column) {
                    $columns[$i][] = trim($column);
                }
            }

        }
        unset($row);

        $commonType = array('table' => array('row' => array()));
        foreach ($columns as &$column) {
            $commonType['table']['row'][] = array('column' => $column);
        }
        unset($column);

        return $commonType;
    }

    public function convertToConverterType($commonType)
    {
        $columns = $this->getColumns($commonType);
        $columnsWidths = $this->getColumnsWidths($columns);

        $tableAsciiType = $this->getTableLine($columnsWidths);
        $tableAsciiType .= $this->getTableRow($columns[0], $columnsWidths);
        $tableAsciiType .= $this->getTableLine($columnsWidths);

        for ($i = 1; $i < count($columns); ++$i) {
            $tableAsciiType .= $this->getTableRow($columns[$i], $columnsWidths);
        }

        $tableAsciiType .= $this->getTableLine($columnsWidths);

        return $tableAsciiType;
    }

    private function getColumns($commonType)
    {
        $columns = array();
        foreach ($commonType['table']['row'] as $i => $row) {
            $columns[$i] = $row['column'];
        }

        return $columns;
    }

    private function getColumnsWidths($columns)
    {
        $widths = array();
        for ($i = 0; $i < count($columns[0]); ++$i) {
            $widths[$i] = 0;
        }

        foreach ($columns as $row) {
            foreach ($row as $j => $cellValue) {
                $cellValueLength = strlen($cellValue);
                if ($cellValueLength > $widths[$j]) {
                    $widths[$j] = $cellValueLength;
                }
            }
        }

        return $widths;
    }

    private function getTableLine($columnsWidths)
    {
        $asciiRowLine = '+';

        foreach ($columnsWidths as $columnWidth) {
            $asciiRowLine .= str_repeat('-', $columnWidth + 2) . '+';
        }

        return $asciiRowLine . PHP_EOL;
    }

    private function getTableRow($rowData, $columnsWidths)
    {
        $rowLine = '|';
        foreach ($rowData as $i => $cellValue) {
            $emptyCharsCount = $columnsWidths[$i] - strlen($cellValue);
            $emptyChars = $emptyCharsCount > 0 ? str_repeat(' ', $columnsWidths[$i] - strlen($cellValue)) : '';

            $rowLine .= " {$cellValue}{$emptyChars} |";
        }

        return $rowLine . PHP_EOL;
    }
}