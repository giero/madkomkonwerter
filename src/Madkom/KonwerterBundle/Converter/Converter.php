<?php

namespace Madkom\KonwerterBundle\Converter;

use Madkom\KonwerterBundle\Entity\Conversion;

class Converter
{
    private $converters = array();

    /**
     * Dodawanie nowych konwerterow, na ktorych bedziemy dalej operowac.
     *
     * @param ConverterInterface $converter
     * @param string $alias
     */
    public function addConverter(ConverterInterface $converter, $alias)
    {
        $this->converters[$alias] = $converter;
    }

    /**
     * Pobranie par 'alias => nazwa' konwerterow.
     *
     * @return array
     */
    public function getConvertersNamesByAlias()
    {
        $convertersNamesByAliases = array();

        /** @var $converter ConverterInterface */
        foreach ($this->converters as $alias => $converter) {
            $convertersNamesByAliases[$alias] = $converter->getName();
        }

        return $convertersNamesByAliases;
    }

    /**
     * Dokonuje konwersji na podstawie rozpoznania typu danych wejsciowych oraz zadanego typu wyjsciowego.
     *
     * @param Conversion $conversion
     * @return string
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function convert(Conversion $conversion)
    {
        $inputData = $conversion->getData();
        $targetType = $conversion->getType();

        /** @var $converter ConverterInterface */
        foreach ($this->converters as $converter) {
            if ($converter->doesInputMatchForType($inputData)) {
                $commonType = $converter->convertToCommonType($inputData);

                return $this
                    ->getConverter($targetType)
                    ->convertToConverterType($commonType);
            }
        }

        throw new \InvalidArgumentException('Nierozpoznawalny typ danych wejscioweych');
    }

    /**
     * Pobieranie konwertera pod aliasie.
     *
     * @param string $alias
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function getConverter($alias)
    {
        if (array_key_exists($alias, $this->converters)) {
            return $this->converters[$alias];
        }

        throw new \InvalidArgumentException(
            "Nieznany alias konwertera: {$alias} - dostępne typy: " . implode(', ', array_keys($this->converters))
        );
    }
}