<?php

namespace Madkom\KonwerterBundle\Controller;

use Madkom\KonwerterBundle\Converter\Converter;
use Madkom\KonwerterBundle\Entity\Conversion;
use Madkom\KonwerterBundle\Form\ConversionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * Strona glowna
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        /** @var Converter $converter */
        $converter = $this->container->get('madkom_konwerter');

        $convertersNamesByAlias = $converter->getConvertersNamesByAlias();

        $conversion = new Conversion();
        $conversionType = new ConversionType();
        $conversionType->setTypeOptions($convertersNamesByAlias);

        $form = $this->createForm($conversionType, $conversion);

        $conversionResult = '';
        $form->handleRequest($request);
        if ($form->isValid()) {
            try {
                $conversionResult = $converter->convert($conversion);
            } catch (\InvalidArgumentException $ex) {
                $conversionResult = $ex->getMessage();
            } catch (\Exception $ex) {
                $conversionResult = 'Bład wewnętrzny konwertera: ' . $ex->getMessage();
            }
        }

        return $this->render(
            'MadkomKonwerterBundle:Default:index.html.twig',
            array(
                'form' => $form->createView(),
                'inputTypes' => $convertersNamesByAlias,
                'conversionResult' => $conversionResult
            )
        );
    }

    /**
     * Akcja zwracajaca przykladowe dane wejsciowe dla konwertera
     *
     * @return JsonResponse
     */
    public function getFixturesAction() {
        /** @var Request $request */
        $request = $this->container->get('request');
        $sampleType = $request->get('type');

        return new JsonResponse($this->getFixtureContent($sampleType));
    }

    /**
     * Pobiera zawartosc pliku z danymi wejsciowymi dla konwertera
     *
     * @param string $alias
     * @return string
     */
    private function getFixtureContent($alias)
    {
        $fixturesPath = __DIR__ . '/../Resources/tests/fixtures/';
        $inputTypes = array(
            'xml' => 'xmlInput.xml',
            'json' => 'jsonType.json',
            'html' => 'tableHtmlInput.html',
            'ascii' => 'tableASCIIType',
            'ul.li' => 'ulLiHtmlInput.html',
            'csv' => 'csvCommaType.csv'
        );

        return file_get_contents($fixturesPath . $inputTypes[$alias]);
    }
}
