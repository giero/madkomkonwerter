<?php

namespace Madkom\KonwerterBundle\Tests\Converters;

use Madkom\KonwerterBundle\Converter\Converters\TableAsciiConverter;

class TableAsciiConverterTest extends AbstractConverterTest
{
    /** @var TableAsciiConverter */
    private $tableConverterConverter;

    protected function setUp()
    {
        parent::setUp();

        $this->tableConverterConverter = $this->container->get('madkom_konwerter.ascii');
    }

    public function testGetName()
    {
        $this->assertEquals('table ASCII', $this->tableConverterConverter->getName());
    }

    public function testDoesInputMatchForType()
    {
        $this->assertTrue($this->tableConverterConverter->doesInputMatchForType($this->inputTypes['ascii']));
    }

    public function testConvertToCommonType()
    {
        $commonType = $this->tableConverterConverter->convertToCommonType($this->inputTypes['ascii']);
        $this->assertInternalType('array', $commonType);
        $this->assertEquals($this->inputTypes['common'], $commonType);
    }

    public function testConvertToConverterType()
    {
        $targetType = $this->tableConverterConverter->convertToConverterType($this->inputTypes['common']);
        // html i xml ma taka sama strukture - mozemy wykorzystac zatem dla html-a asercje xml-a
        $this->assertEquals($this->inputTypes['ascii'], $targetType);
    }
}
 