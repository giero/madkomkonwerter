<?php

namespace Madkom\KonwerterBundle\Converter\Converters;


class JsonConverter extends AbstractConverter
{
    public function doesInputMatchForType($inputData)
    {
        return null !== json_decode($inputData);
    }

    public function convertToCommonType($inputData)
    {
        return json_decode($inputData, true);
    }

    public function convertToConverterType($commonType)
    {
        return json_encode($commonType);
    }
}