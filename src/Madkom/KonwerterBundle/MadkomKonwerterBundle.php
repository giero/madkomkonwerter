<?php

namespace Madkom\KonwerterBundle;

use Madkom\KonwerterBundle\DependencyInjection\Compiler\ConverterCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MadkomKonwerterBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new ConverterCompilerPass());
    }
}
