<?php

namespace Madkom\KonwerterBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ConverterCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('madkom_konwerter')) {
            return;
        }

        $definition = $container->getDefinition('madkom_konwerter');

        $taggedServices = $container->findTaggedServiceIds('madkom_konwerter.converter');
        foreach ($taggedServices as $id => $attributes) {
            if (!isset($attributes[0]['alias'])) {
                throw new \Exception("Brak aliasu dla konwertera '{$id}'");
            }
            $definition->addMethodCall(
                'addConverter',
                array(new Reference($id), $attributes[0]['alias'])
            );
        }
    }
}