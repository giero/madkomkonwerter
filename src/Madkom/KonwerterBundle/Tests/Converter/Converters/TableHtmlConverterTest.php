<?php

namespace Madkom\KonwerterBundle\Tests\Converters;

use Madkom\KonwerterBundle\Converter\Converters\TableHtmlConverter;

class HtmlTableConverterTest extends AbstractConverterTest
{
    /** @var TableHtmlConverter */
    private $tableHtmlConverter;

    protected function setUp()
    {
        parent::setUp();

        $this->tableHtmlConverter = $this->container->get('madkom_konwerter.html');
    }

    public function testGetName()
    {
        $this->assertEquals('table html', $this->tableHtmlConverter->getName());
    }

    public function testDoesInputMatchForType()
    {
        $this->assertTrue($this->tableHtmlConverter->doesInputMatchForType($this->inputTypes['html']));
    }

    public function testConvertToCommonType()
    {
        $commonType = $this->tableHtmlConverter->convertToCommonType($this->inputTypes['html']);
        $this->assertInternalType('array', $commonType);
        $this->assertEquals($this->inputTypes['common'], $commonType);
    }

    public function testConvertToConverterType()
    {
        $targetType = $this->tableHtmlConverter->convertToConverterType($this->inputTypes['common']);
        // html i xml ma taka sama strukture - mozemy wykorzystac zatem dla html-a asercje xml-a
        $this->assertXmlStringEqualsXmlString($this->inputTypes['html'], $targetType);
    }
}
 