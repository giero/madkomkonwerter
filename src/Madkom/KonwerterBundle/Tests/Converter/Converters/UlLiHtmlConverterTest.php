<?php

namespace Madkom\KonwerterBundle\Tests\Converters;

use Madkom\KonwerterBundle\Converter\Converters\UlLiHtmlConverter;

class UlLiHtmlConverterTest extends AbstractConverterTest
{
    /** @var UlLiHtmlConverter */
    private $ulLiHtmlConverter;

    protected function setUp()
    {
        parent::setUp();

        $this->ulLiHtmlConverter = $this->container->get('madkom_konwerter.ul.li');
    }

    public function testGetName()
    {
        $this->assertEquals('ul/li html', $this->ulLiHtmlConverter->getName());
    }

    public function testDoesInputMatchForType()
    {
        $this->assertTrue($this->ulLiHtmlConverter->doesInputMatchForType($this->inputTypes['ul.li']));
    }

    public function testConvertToCommonType()
    {
        $commonType = $this->ulLiHtmlConverter->convertToCommonType($this->inputTypes['ul.li']);
        $this->assertInternalType('array', $commonType);
        $this->assertEquals($this->inputTypes['common'], $commonType);
    }

    public function testConvertToConverterType()
    {
        $targetType = $this->ulLiHtmlConverter->convertToConverterType($this->inputTypes['common']);
        // html i xml ma taka sama strukture - mozemy wykorzystac zatem dla html-a asercje xml-a
        $this->assertXmlStringEqualsXmlString($this->inputTypes['ul.li'], $targetType);
    }
}
 