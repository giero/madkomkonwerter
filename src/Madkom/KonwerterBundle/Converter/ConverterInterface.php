<?php

namespace Madkom\KonwerterBundle\Converter;

/**
 * Interface ConverterInterface
 * @package Madkom\KonwerterBundle\Converter
 */
interface ConverterInterface
{
    public function getName();

    public function doesInputMatchForType($inputData);

    public function convertToCommonType($inputData);

    public function convertToConverterType($commonType);
}